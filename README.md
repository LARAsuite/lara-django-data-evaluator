# LARA-django Data Evaluator

Data evaluation microservice for asynchronously evaluate data in the LARA database.

## Features

## Installation

    pip install lara_django_data_evaluator --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    lara_django_data_evaluator --help 

## Development

    git clone gitlab.com/larasuite/lara-django-data-evaluator

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://larasuite.gitlab.io/lara-django-data-evaluator](https://larasuite.gitlab.io/lara-django-data-evaluator) or [lara-django-data-evaluator.gitlab.io](lara_django_data_evaluator.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



