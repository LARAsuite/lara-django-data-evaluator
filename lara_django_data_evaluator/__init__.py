"""Top-level package for LARA-django Data Evaluator."""

__author__ = """mark doerr"""
__email__ = "mark.doerr@uni-greifswald.de"
__version__ = "0.0.1"
